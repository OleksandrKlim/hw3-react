import { RouterProvider } from "react-router-dom";
import { router } from "./router";

import "./App.scss";
function App({ props }) {
  return (
    <div className="App">
      <RouterProvider router={router}></RouterProvider>
    </div>
  );
}

export default App;
