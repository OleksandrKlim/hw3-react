import React from "react";
// import PropTypes from "prop-types";
import "./Card.scss";
export function Card(product) {
  return (
    <li className="item">
      <h2 className="item__header"> Назва товару:{product.name} </h2>
      <div className="item__wrapper">
        <img src={product.img} alt={product.name} className="item__img" />
      </div>
      <p className="item__prise">Ціна: {product.prise} грн.</p>
      <p className="item__art">Артікул: {product.article}</p>
      <p className="item__color">Колір: {product.color}</p>
      <div>{product.action}</div>
    </li>
  );
}

// Card.propTypes = {
//   className: PropTypes.string,
//   name: PropTypes.string.isRequired,
//   img: PropTypes.string.isRequired,
//   prise: PropTypes.number.isRequired,
//   article: PropTypes.number.isRequired,
//   color: PropTypes.string.isRequired,
// };
