import React from "react";
import { useOutletContext } from "react-router-dom";
import { Card } from "../Card/Card";
import { Button } from "../Button/Button";
import { Svg } from "../Button/Svg";
import { Modal } from "../Modal/Modal";
import "./cardList.scss";

export function CardList(product) {
  const {
    products,
    like,
    setLike,
    basket,
    isModalVisibleFirst,
    setIsModalVisibleFirst,
    isModalVisibleSecond,
    setIsModalVisibleSecond,
    setArticle,
    article,
    setBasket,
  } = useOutletContext();
  const deleteProduct = (article) => {
    const basketCopy = { ...basket };
    delete basketCopy[article];
    setBasket(basketCopy);
  };
  const addToLike = (prodId) => {
    if (like.includes(prodId)) {
      setLike((state) => state.filter((n) => n !== prodId));
    } else setLike((state) => [...state, prodId]);
  };
  const addToBuyClick = (prodId) => {
    if (basket[prodId]) {
      setArticle(prodId);
      setIsModalVisibleSecond(true);
    } else {
      setArticle(prodId);
      setIsModalVisibleFirst(true);
    }
  };
  return (
    <>
      <ul className="cardList">
        {products.map((product) => (
          <Card
            key={product.article}
            name={product.name}
            img={product.img}
            prise={product.prise}
            color={product.color}
            article={product.article}
            action={
              <>
                <Button
                  class="cardList__btn"
                  text="Add to cart"
                  handleClick={() => addToBuyClick(product.article)}
                />

                <Button
                  class="cardList__btn--favorite"
                  text=<Svg
                    color={like.includes(product.article) ? "red" : "white"}
                  />
                  handleClick={() => addToLike(product.article)}
                />
              </>
            }
          />
        ))}
      </ul>
      {isModalVisibleFirst && (
        <Modal
          closeButton={true}
          header="Add purchase to cart ?"
          text="Click ok to add or cancel if you change your mind"
          wrapperClose={(e) => e.stopPropagation()}
          onClose={() => {
            setIsModalVisibleFirst((state) => false);
          }}
          action={
            <>
              <Button
                text="Ok"
                handleClick={() => {
                  setBasket({
                    ...basket,
                    [article]: 1,
                  });

                  setIsModalVisibleFirst((state) => false);
                }}
              />
              <Button
                text="Cancel"
                handleClick={() => {
                  setIsModalVisibleFirst((state) => false);
                }}
              />
            </>
          }
        />
      )}
      {isModalVisibleSecond && (
        <Modal
          closeButton={true}
          header="This item is already in your shopping cart."
          text="Add another one? Click ok to add or delate to remove the item, if you change your mind click the cross"
          wrapperClose={(e) => e.stopPropagation()}
          onClose={() => {
            setIsModalVisibleSecond((state) => false);
          }}
          action={
            <>
              <Button
                text="Ok"
                handleClick={() => {
                  setBasket({
                    ...basket,
                    [article]: basket[article] + 1,
                  });
                  setIsModalVisibleSecond((state) => false);
                }}
              />
              <Button
                text="Delete"
                handleClick={() => {
                  basket[article] === 1
                    ? deleteProduct(article)
                    : setBasket({
                        ...basket,
                        [article]: basket[article] - 1,
                      });
                  setIsModalVisibleSecond((state) => false);
                }}
              />
            </>
          }
        />
      )}
    </>
  );
}
Modal.defaultProps = {
  closeButton: false,
  bakGround: "wrapper",
  wrapper: "modal",
  headerText: "modal__header",
  btvCloth: "modal__cloth",
  bodyText: "modal__bodyText",
};
Button.defaultProps = {
  class: "modal__btn",
};
