import React from "react";
import { useOutletContext } from "react-router-dom";
import { Button } from "../../Button/Button";
import { Card } from "../../Card/Card";
import { Modal } from "../../Modal/Modal";
import { Svg } from "../../Button/Svg";
import "./basket.scss";
function Basket() {
  const {
    basket,
    products,
    setBasket,
    isModalVisibleFirst,
    setIsModalVisibleFirst,
    article,
    setArticle,
    like,
    setLike,
  } = useOutletContext();

  const addToLike = (prodId) => {
    if (like.includes(prodId)) {
      setLike((state) => state.filter((n) => n !== prodId));
    } else setLike((state) => [...state, prodId]);
  };
  const deleteProduct = (article) => {
    const basketCopy = { ...basket };
    delete basketCopy[article];
    setBasket(basketCopy);
  };
  const openModalDeleteProduct = (article) => {
    setArticle(article);
    setIsModalVisibleFirst(true);
  };
  function decrementBasket(article) {
    basket[article] === 1
      ? openModalDeleteProduct(article)
      : setBasket({
          ...basket,
          [article]: basket[article] - 1,
        });
  }
  function incrementBasket(article) {
    setBasket({
      ...basket,
      [article]: basket[article] + 1,
    });
  }

  if (products.length === 0) {
    return null;
  }

  const basketProducts = Object.entries(basket).map(([productId, quantity]) => {
    const product = products.find((product) => {
      return product.article === productId;
    });

    return product;
  });

  const quantity = (productId) => basket[productId];

  return (
    <>
      {Object.keys(basket).length === 0 ? (
        <h2>Корзина порожня. Додайте будьласка товар</h2>
      ) : (
        <ul className="basket">
          {basketProducts.map((el) => (
            <Card
              className="basket__list"
              product={el}
              key={el.article}
              name={el.name}
              img={el.img}
              prise={el.prise}
              color={el.color}
              article={el.article}
              action={
                <>
                  <div>
                    <Button
                      text="+"
                      handleClick={() => incrementBasket(el.article)}
                    />
                    <p className="basket__prise">{quantity(el.article)}</p>
                    <Button
                      text="-"
                      handleClick={() => decrementBasket(el.article)}
                    />
                  </div>
                  <Button
                    class="basket__btn--favorite"
                    text=<Svg
                      color={like.includes(el.article) ? "red" : "white"}
                    />
                    handleClick={() => addToLike(el.article)}
                  />

                  <p className=" basket__sum">
                    Вартість: {quantity(el.article) * el.prise}
                  </p>
                  <div
                    onClick={() => {
                      openModalDeleteProduct(el.article);
                    }}
                    className="basket__clothe"
                  />
                </>
              }
            />
          ))}
        </ul>
      )}
      {isModalVisibleFirst && (
        <Modal
          closeButton={true}
          header="Видалити товар з кошику ?"
          text="Click ok or x to delete or cancel if you change your mind "
          wrapperClose={(e) => e.stopPropagation()}
          onClose={() => {
            setIsModalVisibleFirst((state) => false);
          }}
          action={
            <>
              <Button
                text="Ok"
                handleClick={() => {
                  deleteProduct(article);
                  setIsModalVisibleFirst((state) => false);
                }}
              />
              <Button
                text="Cancel"
                handleClick={() => {
                  setIsModalVisibleFirst((state) => false);
                }}
              />
            </>
          }
        />
      )}
    </>
  );
}

export default Basket;
